# [batch-tools](https://gitlab.com/eidoom/batch-tools/)

## Parallelisation

* Simple example of how to automate parallel submission of jobs to batch, where the jobs are running the same program but with different input parameters. For example, this could be used for evaluating an amplitude at different points in phase space.
* Change the email in `template.sbatch` to your own!
* Simply edit `template.sbatch` such that it runs your script, setting up your script so that it takes command line argument(s) (for the above mentioned input parameters). Then add the input parameters to `looper.sh`.
* Perform the automated submission by running `./looper.sh`.

## Other Slurm commands

* `zquota` shows disk usage
* [`squeue`](https://slurm.schedmd.com/squeue.html) shows all queued jobs. It takes some options
    * `-u USER`
    * `-o "%.18i %.9P %.20j %.8u %.2t %.10M %.6D %R"` makes the name column longer
    * `-i 1` updates the output every second
    * Note that `PD` status means pending
* [`scancel -u USER`](https://slurm.schedmd.com/scancel.html) cancels your queued/running jobs

## Useful linux command examples

* `diff <(squeue -u rmoodie | tail -n +2 | awk '{print $3}' | awk '{print substr ($0, 0, 3)}' | sort) <(seq 000 250)`

## Workstations

Hostname(s)|Threads|CPU|Single-thread performance|Multi-thread performance|Memory (GiB)
---|---|---|---|---|---
ws1,ws2|40|[Intel Xeon E5-2660 v3](https://www.intel.com/content/www/us/en/products/processors/xeon/e5-processors/e5-2660-v3.html)|[1783](https://www.cpubenchmark.net/cpu.php?cpu=Intel+Xeon+E5-2660+v3+%40+2.60GHz&id=2359)|[13132](https://www.cpubenchmark.net/cpu.php?cpu=Intel+Xeon+E5-2660+v3+%40+2.60GHz&id=2359)|125
ws10,ws11|64|[Intel Xeon Gold 6130](https://ark.intel.com/content/www/us/en/ark/products/120492/intel-xeon-gold-6130-processor-22m-cache-2-10-ghz.html)|[1754](https://www.cpubenchmark.net/cpu.php?cpu=Intel+Xeon+Gold+6130+%40+2.10GHz)|[18634](https://www.cpubenchmark.net/cpu.php?cpu=Intel+Xeon+Gold+6130+%40+2.10GHz)|377
?ws3|32|[Intel Xeon E5-2630 v3](https://ark.intel.com/content/www/us/en/ark/products/83356/intel-xeon-processor-e5-2630-v3-20m-cache-2-40-ghz.html)|[1739](https://www.cpubenchmark.net/cpu.php?cpu=Intel+Xeon+E5-2630+v3+%40+2.40GHz)|[9961](https://www.cpubenchmark.net/cpu.php?cpu=Intel+Xeon+E5-2630+v3+%40+2.40GHz)|62
ws3,ws4|40|[Intel Xeon E5-2630 v4](https://www.intel.com/content/www/us/en/products/processors/xeon/e5-processors/e5-2630-v4.html)|[1713](https://www.cpubenchmark.net/cpu.php?cpu=Intel+Xeon+E5-2660+v4+%40+2.00GHz&id=2881)|[14342](https://www.cpubenchmark.net/cpu.php?cpu=Intel+Xeon+E5-2660+v4+%40+2.00GHz&id=2881)|78
?ws5|40|[Intel Xeon E5-2630 v4](https://ark.intel.com/content/www/us/en/ark/products/92981/intel-xeon-processor-e5-2630-v4-25m-cache-2-20-ghz.html)|[1604](https://www.cpubenchmark.net/cpu.php?cpu=Intel+Xeon+E5-2630+v4+%40+2.20GHz)|[11605](https://www.cpubenchmark.net/cpu.php?cpu=Intel+Xeon+E5-2630+v4+%40+2.20GHz)|62
ws6|40|[Intel Xeon E5-2630 v4](https://ark.intel.com/content/www/us/en/ark/products/92981/intel-xeon-processor-e5-2630-v4-25m-cache-2-20-ghz.html)|[1604](https://www.cpubenchmark.net/cpu.php?cpu=Intel+Xeon+E5-2630+v4+%40+2.20GHz)|[11605](https://www.cpubenchmark.net/cpu.php?cpu=Intel+Xeon+E5-2630+v4+%40+2.20GHz)|1007
?d75|8|[Intel Core i7-4790](https://ark.intel.com/content/www/us/en/ark/products/80806/intel-core-i7-4790-processor-8m-cache-up-to-4-00-ghz.html)|[2283](https://www.cpubenchmark.net/cpu.php?cpu=Intel+Core+i7-4790+%40+3.60GHz&id=2226)|[7211](https://www.cpubenchmark.net/cpu.php?cpu=Intel+Core+i7-4790+%40+3.60GHz&id=2226)|15
?d43|8|[Intel Core i7-6700](https://ark.intel.com/content/www/us/en/ark/products/80806/intel-core-i7-6700-processor-8m-cache-up-to-4-00-ghz.html)|[2155](https://www.cpubenchmark.net/cpu.php?cpu=Intel+Core+i7-6700+%40+3.40GHz&id=2598)|[8052](https://www.cpubenchmark.net/cpu.php?cpu=Intel+Core+i7-6700+%40+3.40GHz&id=2598)|15
?d55|8|[Intel Core i7-4770](https://ark.intel.com/content/www/us/en/ark/products/75122/intel-core-i7-4770-processor-8m-cache-up-to-3-90-ghz.html)|[2227](https://www.cpubenchmark.net/cpu.php?cpu=Intel+Core+i7-4770+%40+3.40GHz&id=1907)|[7031](https://www.cpubenchmark.net/cpu.php?cpu=Intel+Core+i7-4770+%40+3.40GHz&id=1907)|15

## Laptops
Model|Threads|CPU|Single-thread performance|Multi-thread performance|Memory (GiB)
---|---|---|---|---|---
Dell XPS 15 9560|8|[Intel Core i7-7700HQ](https://ark.intel.com/content/www/us/en/ark/products/97185/intel-core-i7-7700hq-processor-6m-cache-up-to-3-80-ghz.html)|[2140](https://www.cpubenchmark.net/cpu.php?cpu=Intel+Core+i7-7700HQ+%40+2.80GHz&id=2906)|[6979](https://www.cpubenchmark.net/cpu.php?cpu=Intel+Core+i7-7700HQ+%40+2.80GHz&id=2906)|15
Lenovo Thinkpad X280|8|[Intel Core i5-8350U](https://ark.intel.com/content/www/us/en/ark/products/124969/intel-core-i5-8350u-processor-6m-cache-up-to-3-60-ghz.html)|[2105](https://www.cpubenchmark.net/cpu.php?cpu=Intel+Core+i5-8350U+%40+1.70GHz&id=3150)|[6418](https://www.cpubenchmark.net/cpu.php?cpu=Intel+Core+i5-8350U+%40+1.70GHz&id=3150)|15
