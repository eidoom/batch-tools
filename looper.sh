#!/usr/bin/env bash
# To run jobs on the batch, do:
# ./looper.sbatch

parameters=(0 1 2 3 4 5 6 7 8 9)

for p in ${parameters[@]}
	do
	par=p${p}
	name=${par}.sh
	sed "s/%j/${par}/g" template.srun > $name
	sbatch $name $p
	rm -f $name
	done

